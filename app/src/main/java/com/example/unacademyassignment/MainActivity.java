package com.example.unacademyassignment;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.unacademyassignment.views.CustomProgressBar;

public class MainActivity extends AppCompatActivity {

    EditText editText ;
    Button submitButton ;
    CustomProgressBar customProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.editText);
        submitButton = findViewById(R.id.submitButton);
        customProgressBar = findViewById(R.id.customProgressBar);

    }

    public void onClickSubmit(View view) {
        String value = editText.getText().toString();
        float val = (float) Integer.parseInt(value);
        Log.i("gajanan",val+"");
        customProgressBar.setProgressWithAnimation(val);
    }
}
