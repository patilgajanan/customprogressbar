package com.example.unacademyassignment.views;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import androidx.annotation.Nullable;

public class CustomProgressBar extends View {

    private float strokeWidth = 4;
    private float progress = 0;
    private int min = 0;
    private int max = 100;
    private float sweepAngle = 60.0f;
    private float bobX = 0.0f;
    private float bobY = 0.0f;

    private int startAngle = -90;
    private int backgroundColor = Color.GRAY;
    private int foregroundColor = Color.GREEN;
    private int bobColor = Color.argb(255,255,165,0);
    private RectF rectF;
    private RectF borderF;
    private Paint backgroundPaint;
    private Paint foregroundPaint;
    private Paint bobPaint;
    float radius = 0.0f;
    float center_x = 0.0f;
    float center_y = 0.0f;

    public CustomProgressBar(Context context) {
        super(context);
        init(null);
    }

    public CustomProgressBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public CustomProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }
    private void init (@Nullable AttributeSet set){
        rectF = new RectF();
        borderF = new RectF();

        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(backgroundColor);
        backgroundPaint.setStyle(Paint.Style.STROKE);
        backgroundPaint.setStrokeWidth(strokeWidth);

        foregroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        foregroundPaint.setColor(foregroundColor);
        foregroundPaint.setStyle(Paint.Style.STROKE);
        foregroundPaint.setStrokeWidth(strokeWidth);

        bobPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        bobPaint.setColor(bobColor);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int min = Math.min(width, height);

        radius = min / 2.5f;
        center_x = min / 2;
        center_y = min / 2;

        setMeasuredDimension(min, min);

        rectF.set(center_x - radius, center_y - radius, center_x + radius, center_y + radius);
        borderF.set(0,0,min,min);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        sweepAngle = 360 * progress / max;
        bobX = (float) Math.cos(Math.toRadians(270 + sweepAngle)) * radius + center_x;
        bobY = (float) Math.sin(Math.toRadians(270 + sweepAngle)) * radius + center_y;

        Log.e("Gajanan", startAngle + " --- " + sweepAngle);

        canvas.drawOval(rectF,backgroundPaint);
        canvas.drawArc(rectF, startAngle, sweepAngle,false,foregroundPaint);
        canvas.drawCircle(bobX,bobY,15,bobPaint);
    }

    public void setProgress(float progress){
        this.progress = progress;
        invalidate();
    }

    public void setProgressWithAnimation(float progress) {
        progress = progress % (max+1);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "progress", progress);
        objectAnimator.setDuration(1000);
        objectAnimator.setInterpolator(new DecelerateInterpolator());
        objectAnimator.start();
        invalidate();
    }
}
